# Table of Contents
* [What is thanos?](#what-is-thanos?)
* [Why is thanos?](#why-is-thanos?)
    * [Prometheus cons](#prometheus-cons)
        * [Global query view](#global-query-view)
        * [Merging and deduplicating](#merging-and-deduplicating)
        * [Reliable historical data storage](#reliable-historical-data-storage)
        * [Downsample data](#downsample-data)
    * [Thanos solution](#thanos-solution)
* [When - Some usecases](#when-some-usecases)
* [Architecture](#architecture)
* [Internal](#internal)
    * [Global view](#global-view)
    * [Unlimited retention](#unlimited-retention)
    * [Compactor - Downsampling](#compactor-downsampling)
        * [Compactor](#compactor)
        * [Downsampling](#downsampling)
    * [Recording Rule](#recording-rule)
* [HA - Failover](#ha-failover)
* [Alternative - pros and cons](#alternative-pros-and-cons)
    * [Cortex](#cortex)
    * [VictoriaMetric](#victoriametric)
* [Limitation](#limitation)
* [Demo](#demo)

## What is thanos?
Thanos was started in 2017 by Fabian Reinartz and Bartłomiej Płotka, and joined the CNCF sandbox in 2019. Currently,Thanos is a **CNCF Incubating project**.

Thanos is a **set of components** that can be integrated with Prometheus to strengthen, empower Prometheus. Thanos can **be added seamlessly** on top of existing Prometheus deployments.

![alt text](./img/img-1.svg "CNCF projects")

Thanos leverages the Prometheus 2.0 storage format to cost-efficiently store historical metric data in any **object storage** while retaining fast query latencies. Additionally, it provides a global query view across all Prometheus installations and can merge data from Prometheus HA pairs on the fly.

Concretely the targets of Thanos are:
1. Global query view of metrics.
2. Unlimited retention of metrics.
3. High availability of components, including Prometheus.

## Why is thanos?
Thanos comes into place as a solution for the cons of Prometheus, so we need to relook at Prometheus to see why we need Thanos.

### Prometheus cons

#### Global query view

From the Prometheus side, if we want to enable HA mode, we have some options:
* **Example 1:** Multiple Prometheus to scrape all target's metric.

![alt text](./img/img-2.jpg "Prometheus HA 0")

![alt text](./img/img-3.png "Prometheus HA 1")

As the picture, we can see with multiple Prometheus, we ensure that HA mode is applied to deployment. However, the data view is splitted on Grafana, and Prometheus endpoint as well.

* **Example 2:** Multiple Prometheus to scrape all target's metric behind load balancer.

![alt text](./img/img-4.png "Prometheus HA 2")

With this approach, we only have 1 endpoint that makes us easy to query Prometheus and to setup Grafana datasource. Nevetherless, when we refresh data, we will see that the metrics might potentially differ each other, because they still come from different Prometheus. 

* **Example 3:** Multiple-level Hierarchical Federation

![alt text](./img/img-5.png "Prometheus HA 2")

As the picture, it means setting up a single meta-Prometheus that scraped a portion of the metrics from each child Prometheus. 

However, it leads to an increased configuration, creates an additional potential failure point. In addition, that kind of federation does not allow a truly global view, since not all data is available from a single query API.

#### Merging and deduplicating
As a kind of example-2 above, we can notice that the scraped metric data can be duplicate multiple time (even though we use sharding to increase performance), and it does not have a unified view of data collected by Prometheus HA.

Therefore, a merged and deduplicated view of both data streams is a huge improvement. 

#### Reliable historical data storage
Prometheus stores metric data to its local disk, which limits the data we can query or otherwise process to the most recent days or weeks. And while highly-efficient data compression can get significant mileage out of a local SSD, there is ultimately a limit on how much historical data can be stored.

Besides, we need to consider reliability, simplicity and cost. Large local disks are harder to operate and backup, they are more expensive and require tools for backup which uncecessary complexity.

#### Downsampled data
Once we started querying historical data, we soon realized that we might make queries slower as we retrieve weeks, months, and ultimately years worth of data.

The usual solution to that problem is called downsampling, a process of reducing the sampling rate of the signal. With downsampled data, we can "zoom out" to a larger time range and maintain the same number of samples, thus keeping queries responsive.

### Thanos solution
By Thanos, we have the clear solution to improve all of cons above, we can also setup Thanos on existing Prometheus without change on configuration. We will discuss more about each component below and how they interact each other, but let's take a quick look at Thanos component's purpose:

- **For global view and deduplicate of data:** Thanos Query and Thanos Sidecar.

- **For long-term storage:** Thanos Storage Gateway with Object storage like AWS S3.

- **For downsampling:** Thanos Compact.

- **For global alerts and rule:** Thanos Ruler.

## When - Some usecases
Some usecases of Thanos:
- When we have a cluster of Prometheus to enable High Availability mode and we want a global of view with unified data.
- When we need to retain Prometheus data in long-term period for some purposes like for analyzing usage trends, for regulatory reason.

## Architecture
![alt text](./img/img-6.png "Thanos architecture")
Thanos consists of the following components:
- **Thanos Sidecar:** 
    * Works as proxy that serves Prometheus local data to Query over gRPC based Store API.
    * This allows Query to get data based on labels as well as time ranges.
    * Uploads the data blocks written by Prometheus to S3 storage.
        * This helps to keep the Prometheus + Sidecar lightweight.

- **Thanos Storage Gateway:** 
    * Retrieves the data from Object Storage System.
    * Participates in gossip cluster and implements Store API.
    * It’s just like another Sidecar for Query.
    * The data blocks on Object Storage System may have several large files. Caching them will also require more storage in Store component. Instead, it tries to align the data and uses an index to minimize the HTTP calls and bandwidth

- **Thanos Query:** 
    * Stateless, horizontally scalable.
    * Works like a Prometheus instance to the outside world; serves same as Prometheus over HTTP API.
    * Queries the Thanos components and Sidecars for data.
    * Deduplication and aggregations happens in the Query
    * Requires unique label pair on all Prometheus instances across cluster so that the deduplication will work correctly. (i.e. if the replica label is different for a metric then it’s a duplicate).

- **Thanos Compact:** 
    * Applies Prometheus’ local compaction mechanism to the historical data from the store.
    * Can be run as a periodic batch job.
    * Down-samples the data to 5 minutes and 1 hour as well as stores basic query results like min, max, sum, count and the Query selects the appropriate result from the data.

- **Thanos Ruler:** 
    * Evaluates rules and alerts against Thanos Query to give a global unified view of data.

***Note:*** Querys, Sidecars and other Thanos components are communicating via **gossip protocol**.

Let's take a quick look at these example below to understand basically each component's function.

**Example 1:** We want to fetch live data from Prometheus cluster that scrape the same target, metric - duplicated data.

![alt text](./img/img-9.png "Thanos eg 1")

**Example 2:** In some specific situation, we don't want only query live data from prometheus-sidecar but the old data from object storage (eg: AWS S3).

![alt text](./img/img-8.png "Thanos eg 2")

**Example 3:** When we want to look over historical data over large time periods (“show me memory pressure over the last year!”), Thanos Compactor component will trawl through GCS/S3, and happily compact and downsample all historical data without change the raw data.

![alt text](./img/img-11.png "Thanos eg 3")


## Internal
Now, here is interesting part, we will deep dive to Thanos component to see how it works.

### Global view
In very first day, we only have Prometheus to scrape data metrics. Now we implement the sidecar next to Prometheus.

![alt text](./img/img-13.png "Thanos internal 1")

We can see the term **"Store API"** everywhere in Thanos document. What is it? To say shortly, it's just a **gRPC** developed by Thanos team to do specific function. In the scope of Sidecar, it remotely read - fetch metric data from Prometheus.

![alt text](./img/img-14.png "Thanos internal 2")

After done for implement Sidecar, we need another component to allow us query metric data and merge/deduplicate metric data from multiple Prometheus. It should also be simple like Promotheus console. That's all the thing Thanos Query handles. 

![alt text](./img/img-16.png "Thanos internal 3")

The flow of this one is: 
-> User A send HTTP request to Thanos Query get the metric data. 
-> Thanos Query fans out to relevant Sidecar by Store API and fetch the data from Prometheus.
-> Thanos Query aggregates the responses, disjoint data.
-> Send back the result to user.

In case User A don't want to see duplicate data, as discussed, Thanos has [a way]() to deduplicate data like below.

![alt text](./img/img-17.png "Thanos internal 4")

After we click deduplicate:

![alt text](./img/img-18.png "Thanos internal 5")

Everything seems perfect for Global view.

### Unlimited retention
Eventually, we would like to store, to backup the historical metric data beyond the Prometheus retention (*default: 15 days, Prometheus automatically remove old data which exceed the retention*) for some purposes with extremely cost efficience. To do this, we use an object storage system that is widely available on cloud (for eg: AWS S3) or even on-premise to backup historical data.

But first, we should understand how Prometheus remotely write to object storage system. Prometheus engine write recent in-memory data to disk every two hours (` storage.tsdb.max-block-duration: default 2h`). A block of persisted data contains several kind:
- One or more chunk files that contains all time series samples.
- Index file which indexes metric names and labels to time series in chunk files. 
- Tombstone files just in case when series are deleted via API, deleted records will be stored there (instead of delete the data immediately from chunk files).

![alt text](./img/img-20.png "Thanos internal 6")

So when the new block appear, the Sidecar at that moment simply watch Prometheus data directory and upload new blocks into an object storage system.

![alt text](./img/img-19.png "Thanos internal 7")

And because Sidecar upload every new blocks as soon as it appears (written to disk), we keep the `scrape` between Prometheus and Sidecar lightweight which simplify maintenance, cost and system design.

So...that's how Thanos component upload data into object storage system to backup. But how can we retrieve data beyond Prometheus retention, like 6-9 months ago? That's when **Thanos Storage Gateway** steps in the game.

![alt text](./img/img-21.png "Thanos internal 8")

Thanos Storage Gateway acts as the data retrieval proxy for data inside object storage system.

The flow is: 
->Thanos Query get HTTP request "query metric A 6-9 months ago".
-> Thanos Query implement Store API to Thanos Storage Gateway.
-> Thanos Storage Gateway retrieve data from Object Storage System and send back to Query.

But another question popup: How to deal with large file in the past? Download from HTTP? Or caching object file? Seems not efficient..

Thanos Storage Gateway has a smart way, it minimize the number of request to object storage system by filtering relevant block (by timerange and label) or caching frequently index of block.

Besides, The Prometheus 2.0 storage layout is optimized for minimal read amplification. For example, sample data for the same time series is sequentially aligned in a chunk file. Similarly, series for the same metric name are sequentially aligned as well. 

![alt text](./img/img-22.png "Thanos internal 9")

### Compactor - Downsampling
#### Compactor
Prometheus TSDB is designed kinda similar to the LSM storage engines – the head block is flushed to disk periodically, while at the same time, compactions to merge a few blocks together are performed to avoid need to scan too many blocks for queries

So at Object Storage System, we also need something to do that kind of thing - compacting data. Thanos has component called Thanos Compactor who takes this responsibility. 

![alt text](./img/img-23.png "Thanos internal 10")


#### Downsampling
Let's talk a little bit about downsample. Imagine we have a metric data with 10.000 data points and we show all data point in graph below:

![alt text](./img/img-24.png "Thanos internal 11")

The result of rendering data contains many more points than the number of pixels for the given canvas width and it's kind of squashed. When visualizing a large amount of data as a line chart, if it's required to view the chart in its entirety then it's okay. 

But with some data, it might be acceptable to average out some data points, creating new data points to represent a group of points in the original data.

![alt text](./img/img-25.png "Thanos internal 12")

Back to main topic, Thanos support for downsampling by compactor component:
- Creating 5m downsampling for blocks larger than 40 hours (2d, 2w)
- Creating 1h downsampling for blocks larger than 10 days (2w).

To produce downsampled data, the Compactor continuously aggregates series down to five minute and one hour resolutions. For each raw chunk, encoded with TSDB’s XOR compression, it stores different types of aggregations, e.g. min, max, or sum in a single block. This allows Query to automatically choose the aggregate that is appropriate for a given PromQL query.

However, please be noticed that the goal of downsampling is not saving diskspace, but in fact it creates 2 more blocks for each raw block which are smaller or similar size to raw block. It means that downsampling can increase the size of storage (~ x3) but gives massive advantage on querying long ranges.

![alt text](./img/img-26.png "Thanos internal 11")

### Recording Rule
It is perfectly fine to keep existing rules and alert at Prometheus side (alertmanager). However alertmanager is not enough to handle these cases:
- Global alerts and rules (e.g: alert when service is down in more than two of three clusters).
- Rules beyond a Prometheus’s local data retention.
- The desire to store all rules and alerts in a single place.

Thanos has a component called Thanos Ruler to handle these cases above. It evaluates rules and alerts against Thanos Querys. Later, they are backed up in Object Storage System and accessible via Storage Gateway.

![alt text](./img/img-27.png "Thanos internal 12")

## HA - Failover
Store, rule, and compactor nodes are all expected to scale significantly within a single instance or high availability pair. Scaling of storage capacity is ensured by relying on an external object storage system.

![alt text](./img/img-28.png "Thanos internal 12")

## Alternative - pros and cons
### Cortex
Cortex is a horizontally scalable, clustered Prometheus implementation aimed at giving users a global view of all their Prometheus metrics in one place, and providing long term storage for those metrics.

![alt text](./img/img-30.png "Cortex")

Now let's compare Cortex with Thanos to see their differences:

<ins>**Global view:**</ins>

**Thanos:** Thanos Query fans out queries to Prometheus servers via Sidecar coming along them.

**Cortext:** Prometheus server push data to a central, scalable Cortex cluster which stores all the data and handles queries locally.

**=> Differences and tradeoffs:** It's about query perfomance and availability. For Thanos, if there is a network interruption at the query moment, query performance and availability will suffer. For Cortex at the same situation, because all data store centrally in Cortex cluster, it's ok to query. On other hand, we need to manage Cortex clusters and storage (remote-write) on Prometheus configuration. With Thanos, they're stateless and only leverage the existing Prometheus (nothing changed on Prometheus configuration).

<ins>**HA and duplicated data:**</ins>

**Thanos:** Thanos Query deduplicate data in query time.

**Cortext:** Cortex deduplicate data on ingestion by storing data from one Prometheus of cluster and then "elect" another Prometheus in cluster to store next time. In case the "master" get problem (timeout 30s), Cortext will store another one in cluster.

<ins>**Long-term storage:**</ins>

**Thanos:** As discussed, Thanos Sidecar sync TSDB block (including Chunk and Index) to Object Storage System.

**Cortext:** It's different a little bit, Cortext only stores Chunk data in Object Storage System, the index part will store on NoSQL database. 

**=> Differences and tradeoffs:** It's about cost and performance. Absolutely, for Cortex we need a NoSQL database to store index which increases more cost than Thanos, but by combining Object Storage System & NoSQL, it can bring to us better performance.


## Limitation
- Prometheus integration only.
- With multi components, there's a risk for consistency.
- Not allow specifying authorization or TLS for Thanos server HTTP APIs yet.

## Demo
The [demo](./demo/README.md) focus on:

- Setup a Thanos set including Sidecar, Query, Storage Gateway, Compactor with existing Prometheus cluster.