# Table of content
* [Cloud native](#cloud-native)
* [Sidecar](#sidecar)
    * [What is sidecar?](#what-is-sidecar?)
    * [Some usecase](#some-usecase)
* [Gossip protocol](#gossip-protocol)
* [gRPC](#grpc)
* [LSM storage engine](#lsm-storage-engine)
* [XOR-based compression](#xor-based-compression)

LSM storage engine

## Cloud native
Definition from CNCF:

> Cloud native technologies empower organizations to build and run scalable applications in modern, dynamic environments such as public, private, and hybrid clouds. Containers, service meshes, microservices, immutable infrastructure, and declarative APIs exemplify this approach.
> These techniques enable loosely coupled systems that are resilient, manageable, and observable. Combined with robust automation, they allow engineers to make high-impact changes frequently and predictably with minimal toil.
>The Cloud Native Computing Foundation seeks to drive adoption of this paradigm by fostering and sustaining an ecosystem of open source, vendor-neutral projects. We democratize state-of-the-art patterns to make these innovations accessible for everyone.

## CNCF project
The technologies that the CNCF researches and creates are called projects, many of which are hosted on GitHub. CNCF projects progress through three stages of maturity:
- The sandbox stage, which is the initial entry point for projects;
- The incubating stage, in which a project must meet all sandbox requirements, as well as additional criteria, such as documentation of at least three end users successfully deploying the project in production;
- The graduation phase, in which a project must meet all requirements from the incubating stage, plus additional criteria, such as the completion of an independent security audit.

## Sidecar
### What is sidecar?
The definition of sidecar pattern is very simple. In the container group like automic container group (as POD in K8S), there are 2 containers: 1 is application container and 2 is sidecar container. The role of sidecar container is to strengthen, to support or to help application container for specific task but don't know details about application container.

Sidecar container is scheduled with application container through a automic container group as POD API object in K8S.

Besides, application container and sidecar container share several resource like filesystem, hostname, network,...

### Some usecase
1. Implement HTTPS for legacy service.

For example you have an very old web service (source code is not supported by provider anymore) that serves for internal network, and because of its purpose, it only serves for HTTP request. Recently, for high network security, you've been told that need to upgrade that web service - HTTPS.

Dockerizing this web services is simple, but addon feature HTTPS is very difficult. But it's simple with sidecar pattern.

Legacy web service is dockerized and configured for only serving request from localhost, means that only services using shared local network have the access to this web service. Therefore, we implement sidecar nginx that accept HTTPS request and redirect to HTTP request of web service. Nginx sidecar container play SSL proxy role.

2. Dynamic config with Sidecar

In cloud-native environment, it's useful to let API update config for applications. It makes stuff like rollback, reconfiguring automated, safe and simple.

In this example, we will have 2 containers: 1 is application and 2 is config manager - sidecar, and both of them locate in a POD where they share 1 directory including config file.

The scenario is:
* Application starts and load config
* Config manager starts and check config if it's different. If yes, it will download new config and send a signal to application to reload config (restart or kill application container)

## Gossip protocol
Gossip Protocol is a communication protocol, it is process computer to computer communication that works on the same principle as how information is shared on social networks. Nowadays, most of the systems often use gossip protocols to solve problems that might be difficult to solve in other ways, either due to inconvenience in the structure, is extremely large, or because gossip solutions are the most efficient ones available.

The modern distributed systems use this peer to peer gossip protocol to make sure that the information is disseminated to all the members in the network.

## gRPC (Google RPC framework)
gRPC is a modern open source high performance **RPC framework** that can run in any environment. It can efficiently connect services in and across data centers with pluggable support for load balancing, tracing, health checking and authentication. It is also applicable in last mile of distributed computing to connect devices, mobile applications and browsers to backend services.

gRPC is a CNCF incubating project.

## LSM storage engine
In computer science, the log-structured merge-tree (or LSM tree) is a data structure with performance characteristics that make it attractive for providing indexed access to files with high insert volume, such as transactional log data. LSM trees, like other search trees, maintain key-value pairs. LSM trees maintain data in two or more separate structures, each of which is optimized for its respective underlying storage medium; data is synchronized between the two structures efficiently, in batches.

## XOR-based compression
First, it's floating point compression, and floating point numbers are generally more difficult to compress than integers. Unlike fixed-length integers which often have a fair number of leading 0s, floating point numbers often use all of their available bits, especially if they are converted from decimal numbers, which can’t be represented precisely in binary.

Basically, it is kind of delta of IEEE754 type. It means we convert floating point to Double IEE754 (64bit) and calculate the delta. According to Facebook, with this compression algorithm, over 50% of floating point values (all doubles) were compressed to a single bit, ~30% to 26.6 bits, and the remainder to 39.6 bits.