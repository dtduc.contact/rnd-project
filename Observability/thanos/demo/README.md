# Table of content
* [Introduction](#introduction)
* [Requirement](#requirement)
* [Implement a Prometheus cluster](#implement-a-prometheus-cluster)
* [Update Prometheus cluster configuration](#update-prometheus-cluster-configuration)
* [Deploy Thanos Query](#deploy-thanos-query)
* [Deploy Thanos Store](#deploy-thanos-store)
* [Deploy Thanos Compactor](#deploy-thanos-compactor)
* [Deploy Thanos Ruler](#deploy-thanos-ruler)

## Introduction
The concept of this demo is to add Thanos seamlessly into **existing Prometheus** and see how Thanos works. The environement in this demo is GCP.

## Requirement
- Google Storage
- Service account credential (Optional - to be used by Thanos components to access object store)
- Ingress
- cert-manager
- Helm

## Implement a Prometheus cluster
First, we need to create a prometheus cluster, please follow these steps below:

- Put your GCP SA credential into credential_sa directory.

- Update your information in `env` file to set the parameters:
```
PROM_URL=prometheus.example.com
GRAFANA_URL=grafana.example.com
QUERY_URL=thanos.example.com
OBJECT_STORE=gcp-storage-example
CREDENTIAL_NAME=thanos-example.json
```

- Execute this command:
```
make init
```

- Output:
```
NAME                                             READY   STATUS    RESTARTS   AGE
grafana-0                                        1/1     Running   0          35m
prometheus-alertmanager-d47577c4b-dps9s          2/2     Running   0          35m
prometheus-kube-state-metrics-6df5d44568-sp45l   1/1     Running   0          35m
prometheus-node-exporter-7phs9                   1/1     Running   0          35m
prometheus-node-exporter-dz8jl                   1/1     Running   0          35m
prometheus-node-exporter-kbd4d                   1/1     Running   0          35m
prometheus-node-exporter-s297g                   1/1     Running   0          35m
prometheus-pushgateway-57c97d878d-7mvdv          1/1     Running   0          35m
prometheus-server-0                              2/2     Running   0          35m
prometheus-server-1                              2/2     Running   0          34m
prometheus-server-2                              2/2     Running   0          34m

```

After these steps, we will have a Prometheus cluster to test.

## Update Prometheus cluster configuration

**1. Update ConfigMap - prometheus.yml:**
- Thanos Sidecar will add an external label for each Prometheus server Pod to detect which metrics for which replica in Prometheus StatefulSet.
```
    global:
      evaluation_interval: 1m
      scrape_interval: 1m
      scrape_timeout: 10s
########### ADD ###############
      external_labels:
        cluster: prometheus-ha
        replica: $(POD_NAME)
###############################
```

**2. Update Prometheus StatefulSet - Adding Sidecar and its config:**

- Add an label thanos-store-api into metadata, by this, Sidecar and Thanos Storage Gateway get discovered by the headless service which will be used by Thanos Query.
```
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: prometheus
        chart: prometheus-11.12.0
        component: server
        heritage: Helm
        release: prometheus
############## ADD #################
        thanos-store-api: "true"
####################################
```

- Change config-file-path of Prometheus to a shared directory, Sidecar will get the file prometheus.yml from ConfigMap and then shared with prometheus including variable $POD_NAME. And of course we need to change mountPath too.
- Besides, Thanos will handle data compaction and therefore we need to set `--storage.tsdb.min-block-duration=2h` and `--storage.tsdb.max-block-duration=2h` to disable local compaction on order to use Thanos sidecar upload.

```
      - args:
        - --storage.tsdb.retention.time=15d
########### CHANGE #############
        - --config.file=/etc/prometheus-shared/prometheus.yml
################################
        - --storage.tsdb.path=/data
        - --web.console.libraries=/etc/prometheus/console_libraries
        - --web.console.templates=/etc/prometheus/consoles
        - --web.enable-lifecycle
########### ADD #############
        - --storage.tsdb.no-lockfile
        - --storage.tsdb.min-block-duration=2h
        - --storage.tsdb.max-block-duration=2h
#############################
...
########### CHANGE #############
        - mountPath: /etc/prometheus-shared
          name: prometheus-config-shared
################################
```

- And then we add the Sidecar container into Pods:
    - `--objstore.config` contains Object Storage System configuration.
    - Thanos sidecar can watch `--reloader.config-file` configuration file, evaluate environment variables found in there $(POD_NAME), and produce generated config in `--reloader.config-envsubst-file`.
    - We create 2 environment variable: `$(POD_NAME)` and `$(GOOGLE_APPLICATION_CREDENTIALS)`.
- We also need to update volume to correct the configuration.
```
########## ADD #############
      - name: thanos
        args:
        - sidecar
        - --log.level=debug
        - --tsdb.path=/data
        - --prometheus.url=http://127.0.0.1:9090
        - '--objstore.config={type: GCS, config: {bucket: <OBJECT_STORE>}}'
        - --reloader.config-file=/etc/config/prometheus.yml
        - --reloader.config-envsubst-file=/etc/prometheus-shared/prometheus.yml
        - --reloader.rule-dir=/etc/prometheus/rules/
        env:
        - name: POD_NAME
          valueFrom:
            fieldRef:
              fieldPath: metadata.name
        - name: GOOGLE_APPLICATION_CREDENTIALS
          value: /etc/secret/thanos-gcs-credentials.json
        image: quay.io/thanos/thanos:v0.8.0
        imagePullPolicy: IfNotPresent
        livenessProbe:
          failureThreshold: 3
          httpGet:
            path: /-/healthy
            port: 10902
            scheme: HTTP
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 1
        ports:
        - containerPort: 10902
          name: http-sidecar
          protocol: TCP
        - containerPort: 10901
          name: grpc
          protocol: TCP
        readinessProbe:
          failureThreshold: 3
          httpGet:
            path: /-/ready
            port: 10902
            scheme: HTTP
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 1
        resources: {}
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
        volumeMounts:
        - mountPath: /data
          name: storage-volume
        - mountPath: /etc/prometheus/rules
          name: config-rules
        - mountPath: /etc/config
          name: config-volume
        - mountPath: /etc/secret
          name: thanos-gcs-credentials
        - name: prometheus-config-shared
          mountPath: /etc/prometheus-shared/
####################################
...
      volumes:
      - configMap:
          defaultMode: 420
          name: prometheus-server
        name: config-volume
############# ADD #############
      - configMap:
          defaultMode: 420
          name: prometheus-alertmanager
        name: config-rules
      - name: thanos-gcs-credentials
        secret:
          defaultMode: 420
          secretName: thanos-gcs-credentials
      - name: prometheus-config-shared
        emptyDir: {}
###############################
```

- Output:
```
prometheus-server-0                              3/3     Running   1          3m10s
prometheus-server-1                              3/3     Running   0          4m6s
prometheus-server-2                              3/3     Running   1          5m8s
```

## Deploy Thanos Query
- We will create a headless service for Sidecar and Store, by this one, Thanos Query can easily detect Sidecar and Store.
```
apiVersion: v1
kind: Service
metadata:
  name: thanos-store-gateway
  namespace: prometheus
spec:
  type: ClusterIP
  clusterIP: None
  ports:
    - name: grpc
      port: 10901
      targetPort: grpc
  selector:
    thanos-store-api: "true"
```

- And then we create a Thanos Query, the template is on thanos directory, I only notice some of them:
    - `--query.replica-label=replica`: to distinguish Prometheus replica data by external label replica.

    - `--store=dnssrv+thanos-store-gateway:10901`: helps discover all the components from which metric data should be queried.
```
      containers:
      - name: thanos
        image: quay.io/thanos/thanos:v0.8.0
        args:
        - query
        - --log.level=debug
        - --query.replica-label=replica
        - --store=dnssrv+thanos-store-gateway:10901
```

- Deploy all above by command:
```
make query
```

## Deploy Thanos Store
- `--data-dir=/data`: Local data directory used for caching purposes (index-header, in-mem cache items and meta.jsons). If removed, no data will be lost, just store will have to rebuild the cache. NOTE: Putting raw blocks here will not cause the store to read them. For such use cases use Prometheus + sidecar.
- `--index-cache-size` Maximum size of items held in the in-memory index cache.
- `--chunk-pool-size=500MB`: Maximum size of concurrently allocatable bytes reserved strictly to reuse for chunks in memory.
```
      containers:
        - name: thanos
          image: quay.io/thanos/thanos:v0.8.0
          args:
          - "store"
          - "--log.level=debug"
          - "--data-dir=/data"
          - "--objstore.config={type: GCS, config: {bucket: objectstore}}"
          - "--index-cache-size=500MB"
          - "--chunk-pool-size=500MB"
```

- Deploy it by command:
```
make store
```

## Deploy Thanos Compactor
- Deploy it by command:
```
make compactor
```

## Deploy Thanos Ruler
- `--alertmanagers.url`: Alertmanager replica URLs to push firing alerts. Ruler claims success if push to at least one alertmanager from discovered succeeds.
- `--query`: Addresses of statically configured query API servers (repeatable)
```
      containers:
        - name: thanos
          image: quay.io/thanos/thanos:v0.8.0
          args:
            - rule
            - --log.level=debug
            - --data-dir=/data
            - --eval-interval=15s
            - --rule-file=/etc/thanos-ruler/*.rules.yaml
            - --alertmanagers.url=http://alertmanager:9093
            - --query=thanos-querier:9090
            - "--objstore.config={type: GCS, config: {bucket: objectstore}}"
            - --label=ruler_cluster="prometheus-ha"
            - --label=replica="$(POD_NAME)"
```
- Deploy it by command:
```
make ruler
```

## Conclusion
- It's just a simple demo to integrate Thanos core component with existing Prometheus. In reality, there's several Prometheus cluster which makes configuration more complex. 