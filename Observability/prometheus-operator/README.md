# Table of Contents
[[_TOC_]]

## Overview of prometheus-operator

**Developed by:** coreOS

**Project Status:** beta

**Project concept:** Custom resource definition and controller

**Target Stack:** Prometheus stack

**Environment:** Kubernetes

**Purposes:** Simplify and automate the configuration of Prometheus based monitoring stack for Kubernetes cluster.

**Main features:**
* **Kubernetes Custom Resources:** Use Kubernetes custom resources to deploy and manage Prometheus, Alertmanager, and related components.

* **Simplified Deployment Configuration:** Configure the fundamentals of Prometheus like versions, persistence, retention policies, and replicas from a native Kubernetes resource.

* **Prometheus Target Configuration:** Automatically generate monitoring target configurations based on label.

**Deployment options:** kube-prometheus / helm chart

## What is Kubernetes operator?
Kubernetes operator is a method for packaging, deploying and managing an application with easier than manual installation and operation. The concept of kubernetes operator is like a pair application-specific controller in which the controller uses [Custom resource](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/) to manage application. By using Kubernetes operator, we can setup and operate a complex application in easier way, with high level configuration and setting. It also helps automatic operation like backup, monitoring, scaling, recovering and upgrading, especially for Stateful Application.

However, same as others tools or methods, Kubernetes operator have a "weakness": because Kubernetes operator is considered as `"wrapper method"` high-level configuration, It might not have all the features or might not be up-to-date as "source technology".

**In summary:**
* Kubernetes operator is a **METHOD** to package, deploy, manage an application.

* Using the concept **Application - Specific controller** in which controller uses Custom Resource to manage Application

* `Pros:`
High-level configuration & settings.
Automatically operating for Application, especially Stateful Application.
* `Cons:`
-Same as wrapper method, It might not be up-to-date or lack of the features as source technology.

## What is prometheus-operator?
To directly translate: It is kubernetes operator for prometheus stack to help peoples deploy and manage in easier way.

Prometheus-operator is developed by coreOS since 2016 and in a way to become the popular choice for people when deploying the prometheus stack on Kubernetes.

The core of prometheus-operator are:

**[1. Prometheus-operator](https://quay.io/repository/coreos/prometheus-operator?tag=latest&tab=tags):**
- Keep detecting new services, pods to be monitored by Custom resource as Servicemonitor, Podmonitor with an familiar way as Kubernetes environment - matchLabel. 
- Manage its own Custom resources as Prometheus, Alertmanager, apply `new` services, `new` pods, `new` rule to monitor and trigger.

**2. Custom Resource Definition:**
- **`Prometheus:`** Define the desired prometheus deployment. Prometheus-operator ensures prometheus deployment using CRD to automatically configure.
- **`Prometheusrule:`** Define the Alert rule to trigger.
- **`Alertmanager:`** Same as Prometheus CRD but for Alert Manager.
- **`Thanosruler:`** Same as Prometheus CRD but for Thanos ruler.
- **`Servicemonitor:`** Define how group of services monitored by using the most familiar method on Kubernetes - Label. Prometheus operator then automatically generates scrape configuration.
- **`Podmonitor:`** Same as Servicemonitor but for Pod.

## Why is prometheus-operator?
In traditional way, while using Prometheus, we need to manually update Prometheus configuration file with static parameters which leads us sometime to hardcore situation to operate and manage, especially for a complex architectures.

Therefore, we need a `autodiscover mechanism` to help us to automatically detect the services then easily monitor them. There are some `autodiscover mechanisms` out there:
- Consul.
- Kubernetes SD plugin.
- Prometheus-operator.

Consul & Kubernetes SD plugin can help us discover the service running on Kubernetes and apply to Prometheus configuration. However, we want something more than that, there should be a method, a tool to let us set up easily the whole Prometheus stack that includes Alert Manager, Grafana and related components, and operate this whole stack in the most easy way. And for all those requirements, Prometheus-operator seems to be the best option.

## When do we use Prometheus-operator - common usecases?
With all the advantages we describe above, here are some common usecase:
- Build a whole Prometheus Stack from scratch `on Kubernetes environment`.
- For big architecture `on Kubernetes` with many services, pods and managed strictly by label.
- For general project - architecture `on Kubernetes` which does not need lates features of Prometheus.

## Architecture
The Architecture of Prometheus-operator is quite simple:

![alt text](./img/img-1.png "Prometheus Operator Architecture")


**1. Prometheus operator:** is a heart, a brain of whole concept which detects, identifies the monitoring configuration of services `(servicemonitor)` and pods `(podmonitor)` then apply to its prometheus `(created by prometheus CRD)` then follows the rules `(prometheusrules)` to trigger alert by Alert Managager `(created by alertmanager CRD)`.

**2. Prometheus (CRD):** acts as normal Prometheus but keep communicating with prometheus-operator to get the configuration through servicemonitor or podmonitor.

**3. Alertmanager (CRD):** acts as normal Alert Manager but has some modifications 

**4. Servicemonitor (CRD):** a Custom resource to identfy which service should be monitored by using matchLabel.

**5. Podmonitor (CRD):** a Custom resource to identify which pod should be monitored by using matchLabel.

**6. Prometheusrule (CRD):** a Custom resource to describe prometheus rule in configuration file.

## High Availability & Failover
Regarding all documents I researched, prometheus-operator concept provides a high availability modes when setting the number of replica to two or more for prometheus and alertmanager resources. Let's take a look at core components of this concept:

### Prometheus operator 
Prometheus operator does not need HA mode because its main job is to checkout any `new` configurations (servicemonitor, podmonitor, prometheusrule) and then apply them. So in case we lose it in short time until a new pod is spawned, nothing will happened except that `new` configurations are not picked up immediately. 

### Prometheus
When we set the number of replica to two or more for prometheus resource, we ensure that more than 1 prometheus scrape metrics and this resource is running in HA mode. 

However, there are some challenges: first, even though all replica of prometheus have same configuration and target to scrape, the metric data can be different (because the scrape cycles can be slightly different). Second is long-term storage because Prometheus was initially designed for short metrics retention, this is where Thanos comes into play. By Thanos, we can deduplicate metric data and can store data in persistent storage in cloud like S3, Google Storage.

### AlertManager
The Prometheus Operator ensures that Alertmanager clusters are properly configured to run highly available on Kubernetes, and allows easy configuration of Alertmanagers discovery for Prometheus. Simply to set multiple replicas

Since v0.5.0 release, Alermanager implement a gossip protocol to synchronize instances of alertmanager cluster regarding notifications to be sent out, to prevent duplicate notifications.

### Exporters
HA depends on specific exporter, but in case of stateless exporter like `kube-state-metrics`, simply launch multiple replicas behind a service.

## Limitation
Prometheus operator focus on Kubernetes environment with target making prometheus become kubernetes native, so we can't apply this method on other environments.

And as a `wrapper` method, Prometheus operator might not be up to date like original one - Prometheus.

## Alternatives and pros/cons
At the moment, I don't know any others method for high-level configuration as prometheus operator. However, about autodiscover mechanism, prometheus has several options for us to discover services on specific environment. I pick up 2 methods that I think it's related to this topic.

### Prometheus with Consul

**Pros:** Flexible with environement, take advantage of Consul feature.

**Cons:** Consul configuration need to be manage strictly.

### Prometheus with Kubernetes SD plugin.

**Pros:** Powerful with Kubernetes environment.

**Cons:** Cannot use this one for other environment.

## Demo
The [demo](./Demo/README.md) focus on following challenges:

1. Setup Prometheus operator.

2. Setup a service on Kubernets which has metrics output and then use a Servicemonitor to discover and monitor on Prometheus.

3. Setup a service to pull external metric and use a Servicemonitor to monitor on Prometheus.

4. Setup Thanos solution for Prometheus operator.