## Table of content
* [Introduction](#introduction)
* [Endpoint Overview](#endpoint-overview)
    * [1. Insert alert](#insert-alert)
    * [2. Update alert](#update-alert)
    * [3. Query alert](#query-alert)
    * [4. Get ongoing alerts](#get-ongoing-alerts) 
    * [5. Get resolved alerts](#get-resolved-alerts)
* [Technical explain](#technical-explain)
    * [1. Type data](#type-data)
    * [2. Stringers](#stringers)
    * [3. Routing](#routing)
    * [4. Encode/Decode](#encode/decode)
    * [5. Database connection](#database-connection)
    * [6. Database queries](#database-queries)
* [Points to improve](#points-to-improve)


## Introduction
The concept of this demo is to implement an API service (by golang) to handle alerts-queries into an specific database.

The database structure below will be use in this playground and one table is for one project:

| Field       | Type        | Null | Key | Default | Extra |
|-------------|-------------|------|-----|---------|-------|
| ID          | int(5)      | NO   | PRI | NULL    |       |
| Time        | char(20)    | YES  |     | NULL    |       |
| Status      | char(10)    | YES  |     | NULL    |       |
| Severity    | char(10)    | YES  |     | NULL    |       |
| Alert       | char(20)    | YES  |     | NULL    |       |
| Description | char(20)    | YES  |     | NULL    |       |
| Instance    | char(10)    | YES  |     | NULL    |       |

## Endpoint Overview
There are 5 API endpoint to handle several features:

| Method |           Endpoint       |  Description  |
|--------|--------------------------|---------------|
|**POST**|        /alert/input      |to insert alert|
|**POST**|        /alert/update     |to update alert|
|**POST**|        /alert/query      |to query alert |
|**GET** | /alert/ongoing/{project} |to check recent alerts (max: 50 alert) with status Ongoing for specific project.|
|**GET** | /alert/resolve/{project} |to check recent alerts (max: 50 alert) with status Resolved for specific project. |

### Insert alert
**Endpoint:** `POST /alert/input`

**Purpose:** Insert alert into table.

**Parameters:** 

|Name        |Type  |
|------------|------|
|Project     |string|
|ID          |string|
|Time        |string|
|Instance    |string|
|Status      |string|
|Severity    |string|
|Alert       |string|
|Description |string|

**Response:** 
- OK response
```
Successfully insert!
```

- KO response
```
Cannot insert alert, please recheck ID!!!
```

### Update alert
**Endpoint:** `POST /alert/update`

**Purpose:** Update alert.

**Parameters:** 

|Name   |Type  |
|-------|------|
|Project|string|
|ID     |string|
|Status |string|

**Response:** 
- OK response
```
Successfully update!
```

- KO response
```
Cannot update alert!!!
```

### Query alert
**Endpoint:** `POST /alert/query`

**Purpose:** query alert.

**Parameters:** 

|Name   |Type  |
|-------|------|
|Project|string|
|Field  |string|
|Value  |string|

**Response:** 
- OK response
```
[{"ID":"1","Time":"11/02/2020-02:37:16","Status":"Resolved","Severity":"Critical","Alert":"high_cpu","Description":"blablabla","Instance":"0.0.0.0"}]
```

### Get ongoing alerts
**Endpoint:** `GET /alert/ongoing/{project}`

**Purpose:** to check recent alerts (max: 50 alert) with status Ongoing for specific project.

**Response:** 
```
[{"ID":"1","Time":"11/02/2020-02:37:16","Status":"Resolved","Severity":"Critical","Alert":"high_cpu","Description":"blablabla","Instance":"0.0.0.0"}]
```
### Get resolved alerts
**Endpoint:** `GET /alert/resolved/{project}`

**Purpose:** to check recent alerts (max: 50 alert) with status Resolved for specific project.

**Response:** 
```
[{"ID":"5","Time":"11/02/2020-02:37:16","Status":"Ongoing","Severity":"Critical","Alert":"high_cpu","Description":"blablabla","Instance":"0.0.0.0"},{"ID":"4","Time":"11/02/2020-02:37:16","Status":"Ongoing","Severity":"Critical","Alert":"high_cpu","Description":"blablabla","Instance":"0.0.0.0"},{"ID":"3","Time":"11/02/2020-02:37:16","Status":"Ongoing","Severity":"Critical","Alert":"high_cpu","Description":"blablabla","Instance":"0.0.0.0"},{"ID":"2","Time":"11/02/2020-02:37:16","Status":"Ongoing","Severity":"Critical","Alert":"high_cpu","Description":"blablabla","Instance":"0.0.0.0"}]

```

## Technical explain
### Type data
Because in this demo, we have 3 POST APIs with some differences parameters, so setting the type for each body requests is very importance. Here are all the types:
```
type inputData struct {
	Project     string `json: project`
	ID          string `json: id`
	Time        string `json: time`
	Instance    string `json: instance`
	Status      string `json: status`
	Severity    string `json: severity`
	Alert       string `json: alert`
	Description string `json: description`
}

type updateData struct {
	Project string `json: project`
	ID      string `json: id`
	Status  string `json: status`
}

type queryData struct {
	Project string `json: project`
	Field   string `json: field`
	Value   string `json: value`
}

type outputData struct {
	ID          string
	Time        string
	Status      string
	Severity    string
	Alert       string
	Description string
	Instance    string
}
```
### Stringers
I use stringers to let an type have a better output.
```
func (o outputData) String() string {
	return fmt.Sprintf("{ID: %v, Time: %v, Status: %v, Severity: %v, Alert: %v, Description: %v, Instance: %v}", o.ID, o.Time, o.Status, o.Severity, o.Alert, o.Description, o.Instance)
}
```

### Routing
For routing, I use popular package `"github.com/gorilla/mux"`.

And my configuration is:
```
	// Router
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/alert/input", inputAlert).Methods("POST")
	router.HandleFunc("/alert/update", updateAlert).Methods("POST")
	router.HandleFunc("/alert/query", queryAlert).Methods("POST")
	router.HandleFunc("/alert/ko/{project}", getOngoing).Methods("GET")
	router.HandleFunc("/alert/ok/{project}", getResolved).Methods("GET")
```
Besides, I use `mux variable` to set flexible path for GET method
```
	vars := mux.Vars(r)
	project := vars["project"]
```

### Encode/Decode
For this one, I use a popular package - `"encoding/json"`.
Example:
```
	if err := json.NewDecoder(r.Body).Decode(body); err != nil {
		fmt.Println("Could not decode request body", err)
		return
	}
```
### Database connection
For database connection in this demo, I use 2 packages:
- `"database/sql"`
- `"github.com/go-sql-driver/mysql"`

And implement connection at main():
```
	db, err = sql.Open("mysql", DB_USER+":"+DB_PASS+"@tcp("+DB_HOST+":3306)/alerting")
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()
```
DB connection will be close after shutting down service by using `defer`. The error in whole demo mostly use `panic` to handle error.

### Database queries
For INSERT and UPDATE queries, I use function `Exec` of `database/sql`.
```
	if _, err := db.Exec(query); err != nil {
		fmt.Fprintf(w, "Cannot update alert!!!")
		return
	}
```
For SELECT queries, I use function `Query` to query and `loop Next()` to take output data. However, I don't really understand clearly about `loop Next()`.
```	rows, err := db.Query(query)
	alerts := make([]outputData, 0)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()

	for rows.Next() {
		var alert outputData
		if err := rows.Scan(&alert.ID, &alert.Time, &alert.Status, &alert.Severity, &alert.Alert, &alert.Description, &alert.Instance); err != nil {
			panic(err.Error())
		}
		alerts = append(alerts, alert)
		if err := rows.Err(); err != nil {
			panic(err.Error())
		}
	}
```

## Points to improve
1. Database structure is very simple, to quick implementation for this demo. 

**-> Should be more strict.**

2. At the moment, GET alert API is still very simple with only one condition (1 Field 1 Value). 

**-> Should be flexible, more condition.**

3. The query inside main.go is very ugly and hard to read.

**-> Should use template to make it better.**

4. All the functions at the moment is only on 1 file (main.go), it means that it's very difficul to review, read and update code.

**-> Should be decouple it into multiple files.**