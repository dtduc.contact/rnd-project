// Table structure to test API
//+-------------+-------------+------+-----+---------+-------+
//| Field       | Type        | Null | Key | Default | Extra |
//+-------------+-------------+------+-----+---------+-------+
//| ID          | int(5)      | NO   | PRI | NULL    |       |
//| Time        | char(20)    | YES  |     | NULL    |       |
//| Status      | varchar(10) | YES  |     | NULL    |       |
//| Severity    | char(10)    | YES  |     | NULL    |       |
//| Alert       | char(20)    | YES  |     | NULL    |       |
//| Description | char(20)    | YES  |     | NULL    |       |
//| Instance    | char(10)    | YES  |     | NULL    |       |
//+-------------+-------------+------+-----+---------+-------+

package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
)

// Info API port
const PORT string = "30000"

// Info DB
var DB_USER string = os.Getenv("DB_USER")
var DB_PASS string = os.Getenv("DB_PASS")
var DB_HOST string = os.Getenv("DB_HOST")

var db *sql.DB
var err error

// Type data
type inputData struct {
	Project     string `json: project`
	ID          string `json: id`
	Time        string `json: time`
	Instance    string `json: instance`
	Status      string `json: status`
	Severity    string `json: severity`
	Alert       string `json: alert`
	Description string `json: description`
}

type updateData struct {
	Project string `json: project`
	ID      string `json: id`
	Status  string `json: status`
}

type queryData struct {
	Project string `json: project`
	Field   string `json: field`
	Value   string `json: value`
}

type outputData struct {
	ID          string
	Time        string
	Status      string
	Severity    string
	Alert       string
	Description string
	Instance    string
}

// Helper function
func (o outputData) String() string {
	return fmt.Sprintf("{ID: %v, Time: %v, Status: %v, Severity: %v, Alert: %v, Description: %v, Instance: %v}", o.ID, o.Time, o.Status, o.Severity, o.Alert, o.Description, o.Instance)
}

// Function Handler
func inputAlert(w http.ResponseWriter, r *http.Request) {
	// Decode request body
	body := &inputData{}
	if err := json.NewDecoder(r.Body).Decode(body); err != nil {
		fmt.Println("Could not decode request body", err)
		return
	}
	query := "insert into " + body.Project + " (ID, Time, Status, Severity, Alert, Description, Instance) values (" + body.ID + "," + "\"" + body.Time + "\"" + "," + "\"" + body.Status + "\"" + "," + "\"" + body.Severity + "\"" + "," + "\"" + body.Alert + "\"" + "," + "\"" + body.Description + "\"" + "," + "\"" + body.Instance + "\"" + ")"
	if _, err := db.Exec(query); err != nil {
		fmt.Println("Cannot insert alert, please recheck ID!!!")
		fmt.Fprintf(w, "Cannot insert alert, please recheck ID!!!")
		return
	}
	fmt.Println("Successfully insert!")
	fmt.Fprintf(w, "Successfully insert!")
}

func updateAlert(w http.ResponseWriter, r *http.Request) {
	// Decode request body
	body := &updateData{}
	if err := json.NewDecoder(r.Body).Decode(body); err != nil {
		fmt.Println("Could not decode request body", err)
		return
	}
	query := "update " + body.Project + " set Status=" + "\"" + body.Status + "\"" + " where ID=" + body.ID
	if _, err := db.Exec(query); err != nil {
		fmt.Fprintf(w, "Cannot update alert!!!")
		return
	}
	fmt.Fprintf(w, "Successfully update!")
}

func queryAlert(w http.ResponseWriter, r *http.Request) {
	// Decode request body
	body := &queryData{}
	if err := json.NewDecoder(r.Body).Decode(body); err != nil {
		fmt.Println("Could not decode request body", err)
		return
	}
	query := "select * from " + body.Project + " where " + body.Field + "=" + body.Value + " order by ID desc limit 50"
	rows, err := db.Query(query)
	alerts := make([]outputData, 0)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()

	for rows.Next() {
		var alert outputData
		if err := rows.Scan(&alert.ID, &alert.Time, &alert.Status, &alert.Severity, &alert.Alert, &alert.Description, &alert.Instance); err != nil {
			panic(err.Error())
		}
		alerts = append(alerts, alert)
		if err := rows.Err(); err != nil {
			panic(err.Error())
		}
	}
	json.NewEncoder(w).Encode(alerts)
}

func getOngoing(w http.ResponseWriter, r *http.Request) {
	// Set variable for path by Mux feature
	vars := mux.Vars(r)
	project := vars["project"]
	alerts := make([]outputData, 0)
	query := "select * from " + project + " where Status='Ongoing' order by ID desc limit 50"
	rows, err := db.Query(query)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()

	// Playing around but still not understand how it works
	for rows.Next() {
		var alert outputData
		if err := rows.Scan(&alert.ID, &alert.Time, &alert.Status, &alert.Severity, &alert.Alert, &alert.Description, &alert.Instance); err != nil {
			panic(err.Error())
		}
		alerts = append(alerts, alert)
		if err := rows.Err(); err != nil {
			panic(err.Error())
		}
	}
	json.NewEncoder(w).Encode(alerts)
}

func getResolved(w http.ResponseWriter, r *http.Request) {
	// Set variable for path by Mux feature
	vars := mux.Vars(r)
	project := vars["project"]
	alerts := make([]outputData, 0)
	query := "select * from " + project + " where Status='Resolved' order by ID desc limit 50"
	rows, err := db.Query(query)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()

	// Playing around but still not understand how it works
	for rows.Next() {
		var alert outputData
		if err := rows.Scan(&alert.ID, &alert.Time, &alert.Status, &alert.Severity, &alert.Alert, &alert.Description, &alert.Instance); err != nil {
			panic(err.Error())
		}
		alerts = append(alerts, alert)
		if err := rows.Err(); err != nil {
			panic(err.Error())
		}
	}
	json.NewEncoder(w).Encode(alerts)
}

func main() {
	// Connect to DB
	db, err = sql.Open("mysql", DB_USER+":"+DB_PASS+"@tcp("+DB_HOST+":3306)/alerting")
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	// Announcement
	fmt.Println("API server is running on Port " + PORT)

	// Router
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/alert/input", inputAlert).Methods("POST")
	router.HandleFunc("/alert/update", updateAlert).Methods("POST")
	router.HandleFunc("/alert/query", queryAlert).Methods("POST")
	router.HandleFunc("/alert/ko/{project}", getOngoing).Methods("GET")
	router.HandleFunc("/alert/ok/{project}", getResolved).Methods("GET")
	log.Fatal(http.ListenAndServe(":"+PORT, router))
}
