package service

import (
	"../../entities"
	"../repository"
)

type todoUsecase struct {
	TodoRepository repository.TodoUsecase
}

type TodoUsecase interface {
	Get(u []*entities.Todo) ([]*entities.Todo, error)
	GetID(u *entities.Todo, id uint) (*entities.Todo, error)
	Add(u *entities.Todo) (bool, error)
	Update(u *entities.Todo, id uint, status bool) (bool, error)
	DeleteID(u *entities.Todo, id uint) (bool, error)
	Delete(u *entities.Todo, todo string) (bool, error)
}

func NewTodoUsecase(r repository.TodoUsecase) TodoUsecase {
	return &todoUsecase{r}
}

///////////////////////// TodoUsecase methods //////////////////////////////
func (tu *todoUsecase) Get(u []*entities.Todo) ([]*entities.Todo, error) {
	u, err := tu.TodoRepository.ListAll(u)
	if err != nil {
		return nil, err
	}
	return u, nil
}

func (tu *todoUsecase) GetID(u *entities.Todo, id uint) (*entities.Todo, error) {
	b, err := tu.TodoRepository.ListID(u, id)
	if err != nil {
		return nil, err
	}
	return b, nil
}

func (tu *todoUsecase) Add(u *entities.Todo) (bool, error) {
	_, err := tu.TodoRepository.Add(u)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (tu *todoUsecase) Update(u *entities.Todo, id uint, status bool) (bool, error) {
	_, err := tu.TodoRepository.Update(u, id, status)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (tu *todoUsecase) DeleteID(u *entities.Todo, id uint) (bool, error) {
	_, err := tu.TodoRepository.DeleteID(u, id)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (tu *todoUsecase) Delete(u *entities.Todo, todo string) (bool, error) {
	_, err := tu.TodoRepository.Delete(u, todo)
	if err != nil {
		return false, err
	}
	return true, nil
}

/////////////////////////////////////////////////////////////////////////////
