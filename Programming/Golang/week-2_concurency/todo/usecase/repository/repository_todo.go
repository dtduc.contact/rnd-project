package repository

import "../../entities"

type TodoUsecase interface {
	ListAll(u []*entities.Todo) ([]*entities.Todo, error)
	ListID(u *entities.Todo, id uint) (*entities.Todo, error)
	Add(u *entities.Todo) (bool, error)
	Update(u *entities.Todo, id uint, status bool) (bool, error)
	DeleteID(u *entities.Todo, id uint) (bool, error)
	Delete(u *entities.Todo, todo string) (bool, error)
}
