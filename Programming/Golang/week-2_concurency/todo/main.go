package main

import (
	"./common/config"
	"./common/db"
	"./common/delivery"
	"./controller/general"
	_ "./docs"
	"github.com/labstack/echo"
	echoSwagger "github.com/swaggo/echo-swagger"
)

// @title Todo API
// @version 1.0.0
// @description This is a API document for Todo.
// @termsOfService http://swagger.io/terms/

// @contact.name Duc Dinh
// @contact.url http://www.swagger.io/support
// @contact.email dtduc.contact@gmail.com

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host localhost:3000
// @BasePath /

func main() {
	// take configuration
	config.TakeConfig()

	// DB connection
	db := db.ConnectDB()

	b := general.Newbase(db)

	e := echo.New()
	e = delivery.NewDelivery(e, b.NewService())
	e.GET("/swagger/*", echoSwagger.WrapHandler)
	// echo start
	e.Logger.Fatal(e.Start(":" + config.Conf.API.Port))
}
