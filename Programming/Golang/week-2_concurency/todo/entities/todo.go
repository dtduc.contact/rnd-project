package entities

type Todo struct {
	ID     uint   `gorm:"primary_key" json:"id"`
	Todo   string `json:"todo"`
	Status bool   `json:"status"`
}

type AddApi struct {
	Todo   string `json:"todo"`
	Status bool   `json:"status"`
}

type UpdateApi struct {
	ID     uint `gorm:"primary_key" json:"id"`
	Status bool `json:"status"`
}

type DeleteApi struct {
	Todo string `json:"todo"`
}

type DeleteIdApi struct {
	ID uint `gorm:"primary_key" json:"id"`
}

// Export todos type to
func (Todo) TableName() string {
	return "todos"
}
