package db

import (
	"log"

	"../config"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// ConnectDB function to establish DB connection
func ConnectDB() *gorm.DB {
	dsn := "host=" + config.Conf.Db.Host + " user=" + config.Conf.Db.User + " password=" + config.Conf.Db.Pass + " dbname=" + config.Conf.Db.Dbname + " port=" + config.Conf.Db.Port
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	}
	return db
}
