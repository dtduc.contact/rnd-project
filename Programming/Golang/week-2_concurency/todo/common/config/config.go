package config

import (
	"fmt"
	"log"
	"os"

	"github.com/spf13/viper"
)

type config struct {
	Db struct {
		Host   string
		Port   string
		Dbname string
		User   string
		Pass   string
	}
	API struct {
		Port string
	}
}

// Conf variable to export
var Conf config

// TakeConfig will take config from file and environment
func TakeConfig() {
	c := &Conf
	viper.SetConfigName("config")
	viper.SetConfigType("yml")
	viper.AddConfigPath("common/config")
	viper.AutomaticEnv()
	if err := viper.ReadInConfig(); err != nil {
		fmt.Println(err)
		log.Fatalln(err)
	}

	if err := viper.Unmarshal(&c); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
