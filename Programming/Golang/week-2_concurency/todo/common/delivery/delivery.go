package delivery

import (
	"strconv"

	"../../controller/service"
	"../../entities"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

var svc service.Services

// List godoc
// @Summary List all todos
// @Description List all the todo task
// @Tags todos
// @Accept json
// @Produce json
// @Success 200 {object} entities.Todo
// @Router /list [get]
func List(context echo.Context) error {
	return svc.Todo.GetLists(context)
}

// ListID godoc
// @Summary List todo by ID
// @Description List todo by ID
// @Tags todos
// @Accept json
// @Produce json
// @Param id path string true "List Todo ID"
// @Success 200 {object} entities.Todo
// @Router /list/{id} [get]
func ListID(context echo.Context) error {
	entityTodo := new(entities.Todo)
	takeparam := context.Param("id")
	test, err := strconv.ParseUint(takeparam, 10, 0)
	id := uint(test)
	if err != nil {
		return err
	}
	return svc.Todo.GetID(context, entityTodo, id)
}

// Add godoc
// @Summary Add todo
// @Description Add todo
// @Tags todos
// @Accept json
// @Produce json
// @Param Add body entities.AddApi true "Add Todo ID"
// @Success 200 {object} entities.Todo
// @Router /add [post]
func Add(context echo.Context) error {
	entityTodo := new(entities.Todo)
	if err := context.Bind(entityTodo); err != nil {
		return err
	}
	return svc.Todo.Add(context, entityTodo)
}

// Update godoc
// @Summary Update todo
// @Description Update todo
// @Tags todos
// @Accept json
// @Produce json
// @Param Update body entities.UpdateApi true "Update Todo by ID"
// @Success 200 {object} entities.Todo
// @Router /update [put]
func Update(context echo.Context) error {
	entityTodo := new(entities.Todo)
	if err := context.Bind(entityTodo); err != nil {
		return err
	}
	return svc.Todo.Update(context, entityTodo.ID, entityTodo.Status)
}

// DeleteID godoc
// @Summary Delete todo by ID
// @Description Delete todo by ID
// @Tags todos
// @Accept json
// @Produce json
// @Param Delete body entities.DeleteIdApi true "Delete Todo by ID"
// @Success 200 {object} entities.Todo
// @Router /deleteid [delete]
func DeleteID(context echo.Context) error {
	entityTodo := new(entities.Todo)
	if err := context.Bind(entityTodo); err != nil {
		return err
	}
	return svc.Todo.DeleteID(context, entityTodo, entityTodo.ID)
}

// Delete godoc
// @Summary Delete todo by Todo name
// @Description Delete todo by Todo name
// @Tags todos
// @Accept json
// @Produce json
// @Param Update body entities.DeleteApi true "Delete by Todo name"
// @Success 200 {object} entities.Todo
// @Router /delete [delete]
func Delete(context echo.Context) error {
	entityTodo := new(entities.Todo)
	if err := context.Bind(entityTodo); err != nil {
		return err
	}
	return svc.Todo.Delete(context, entityTodo, entityTodo.Todo)
}

// NewDelivery : router for echo framework
func NewDelivery(e *echo.Echo, c service.Services) *echo.Echo {
	svc = c

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.GET("/list", List)

	e.GET("/list/:id", ListID)

	e.POST("/add", Add)

	e.PUT("/update", Update)

	e.DELETE("/deleteid", DeleteID)

	e.DELETE("/delete", Delete)

	return e
}
