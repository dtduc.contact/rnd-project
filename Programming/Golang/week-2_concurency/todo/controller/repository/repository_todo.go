package repository

import (
	"../../entities"
	//"fmt"
	"gorm.io/gorm"
)

type Context interface {
	JSON(code int, i interface{}) error
}

type database struct {
	db *gorm.DB
}

type TodoQuery interface {
	ListAll(u []*entities.Todo) ([]*entities.Todo, error)
	ListID(u *entities.Todo, id uint) (*entities.Todo, error)
	Add(u *entities.Todo) (bool, error)
	Update(u *entities.Todo, id uint, status bool) (bool, error)
	DeleteID(u *entities.Todo, id uint) (bool, error)
	Delete(u *entities.Todo, todo string) (bool, error)
}

func NewTodoQuery(db *gorm.DB) TodoQuery {
	return &database{db}
}

////////////////////////////// TodoQuery //////////////////////////////
func (d *database) ListAll(u []*entities.Todo) ([]*entities.Todo, error) {
	err := d.db.Find(&u).Error
	if err != nil {
		return nil, err
	}
	return u, nil
}

func (d *database) ListID(u *entities.Todo, id uint) (*entities.Todo, error) {
	err := d.db.First(&u, id).Error
	if err != nil {
		return nil, err
	}
	return u, nil
}

func (d *database) Add(u *entities.Todo) (bool, error) {
	if err := d.db.Create(&u).Error; err != nil {
		return false, err
	}
	return true, nil
}

func (d *database) Update(u *entities.Todo, id uint, status bool) (bool, error) {
	if err := d.db.Model(&u).Where("id = ?", id).Update("status", status).Error; err != nil {
		return false, err
	}
	return true, nil
}

func (d *database) DeleteID(u *entities.Todo, id uint) (bool, error) {
	err := d.db.Delete(&u, id).Error
	if err != nil {
		return false, err
	}
	return true, nil
}

func (d *database) Delete(u *entities.Todo, todo string) (bool, error) {
	if err := d.db.Where("todo = ?", todo).Delete(&u).Error; err != nil {
		return false, err
	}
	return true, nil
}

////////////////////////////////////////////////////////////////////////
