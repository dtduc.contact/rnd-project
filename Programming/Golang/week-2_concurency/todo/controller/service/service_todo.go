package service

import (
	"net/http"

	"../../entities"
	us "../../usecase/service"
)

type todoService struct {
	todoUsecase us.TodoUsecase
}

type TodoService interface {
	GetLists(c Context) error
	GetID(c Context, r *entities.Todo, id uint) error
	Add(c Context, r *entities.Todo) error
	Update(c Context, id uint, status bool) error
	DeleteID(c Context, r *entities.Todo, id uint) error
	Delete(c Context, r *entities.Todo, todo string) error
}

func NewTodoService(us us.TodoUsecase) TodoService {
	return &todoService{us}
}

////////////////////////////// TodoService methods //////////////////////////////
func (ts *todoService) GetLists(c Context) error {
	var u []*entities.Todo

	u, err := ts.todoUsecase.Get(u)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, u)
}

func (ts *todoService) GetID(c Context, r *entities.Todo, id uint) error {
	u, err := ts.todoUsecase.GetID(r, id)
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, u)
}

func (ts *todoService) Add(c Context, r *entities.Todo) error {
	_, err := ts.todoUsecase.Add(r)
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, "Successfully insert")
}

func (ts *todoService) Update(c Context, id uint, status bool) error {
	var r *entities.Todo
	_, err := ts.todoUsecase.Update(r, id, status)
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, "Successfully update")
}

func (ts *todoService) DeleteID(c Context, r *entities.Todo, id uint) error {
	_, err := ts.todoUsecase.DeleteID(r, id)
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, "Successfully delete")
}

func (ts *todoService) Delete(c Context, r *entities.Todo, todo string) error {
	_, err := ts.todoUsecase.Delete(r, todo)
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, "Successfully delete")
}

/////////////////////////////////////////////////////////////////////////////////
