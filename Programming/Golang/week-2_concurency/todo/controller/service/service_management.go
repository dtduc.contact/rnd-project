package service

type Services struct {
	Todo interface{ TodoService }
}

type Context interface {
	JSON(code int, i interface{}) error
}
