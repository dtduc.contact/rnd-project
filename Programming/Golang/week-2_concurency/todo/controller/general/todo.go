package general

import (
	uRepository "../../usecase/repository"
	uService "../../usecase/service"
	ctrlRepo "../repository"
	ctrlService "../service"
)

func (b *base) NewTodoService() ctrlService.TodoService {
	return ctrlService.NewTodoService(b.NewTodoUsecase())
}

func (b *base) NewTodoUsecase() uService.TodoUsecase {
	return uService.NewTodoUsecase(b.NewTodoRepository())
}

func (b *base) NewTodoRepository() uRepository.TodoUsecase {
	return ctrlRepo.NewTodoQuery(b.db)
}
