package general

import (
	"../service"
	"gorm.io/gorm"
)

type base struct {
	db *gorm.DB
}

type Base interface {
	NewService() service.Services
}

func Newbase(db *gorm.DB) Base {
	return &base{db}
}

func (b *base) NewService() service.Services {
	return service.Services{
		Todo: b.NewTodoService(),
	}
}
