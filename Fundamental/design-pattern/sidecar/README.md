## Sidecar
### What is sidecar
The definition of sidecar pattern is very simple. In the container group like automic container group (as POD in K8S), there are 2 containers: 1 is application container and 2 is sidecar container. The role of sidecar container is to strengthen, to support or to help application container for specific task but don't know details about application container.

[Image](./img/img-1.jpg)

Sidecar container is scheduled with application container through a automic container group as POD API object in K8S. 

Besides, application container and sidecar container share several resource like filesystem, hostname, network,...

### Why is sidecar


### Some usecase
1. Implement HTTPS for legacy service.

For example you have an very old web service (source code is not supported by provider anymore) that serves for internal network, and because of its purpose, it only serves for HTTP request. Recently, for high network security, you've been told that need to upgrade that web service - HTTPS.

Dockerizing this web services is simple, but addon feature HTTPS is very difficult. But it's simple with sidecar pattern.

[Image](./img/image-2)

Legacy web service is dockerized and configured for only serving request from localhost, means that only services using shared local network have the access to this web service. Therefore, we implement sidecar nginx that accept HTTPS request and redirect to HTTP request of web service. Nginx sidecar container play SSL proxy role.


2. Dynamic config with Sidecar

In cloud-native environment, it's useful to let API update config for applications. It makes stuff like rollback, reconfiguring automated, safe and simple.

In this example, we will have 2 containers: 1 is application and 2 is config manager - sidecar, and both of them locate in a POD where they share 1 directory including config file.

The scenario is: 
* Application starts and load config
* Config manager starts and check config if it's different. If yes, it will download new config and send a signal to application to reload config (restart or kill application container)